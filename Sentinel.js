const fs = require('fs')
const cleaner = (file) => file.replace('.js', '')

const ccPath = `${__dirname}/lib/cc/`
const sfPath = `${__dirname}/lib/sf/`
module.exports = {
  path: {
    cc: ccPath,
    sf: sfPath
  },
  cc: fs.readdirSync(ccPath).map(cleaner),
  sf: fs.readdirSync(sfPath).map(cleaner)
}
