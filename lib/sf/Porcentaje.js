const Type = require('../../validators/Type')

module.exports = (comparacion, porcentaje) => {
  if (Type.isObject(comparacion) && comparacion.hasOwnProperty('unos') && comparacion.hasOwnProperty('ceros') && Type.isNumber(porcentaje)) {
    porcentaje = Math.abs(porcentaje)
    if (porcentaje > 0 && porcentaje <= 100) {
      return Number(((comparacion.unos * 100) / (comparacion.unos + comparacion.ceros)) >= porcentaje)
    } else return 0
  } else return 0
}
