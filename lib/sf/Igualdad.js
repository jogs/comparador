const Type = require('../../validators/Type')
console.log('sf igualdad')
module.exports = (comparacion) => {
  if (Type.isObject(comparacion) && comparacion.hasOwnProperty('ceros')) {
    return Number(comparacion.ceros === 0)
  } else return 0
}
