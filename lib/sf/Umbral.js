const Type = require('../../validators/Type')

module.exports = (comparacion, umbral) => {
  if (Type.isObject(comparacion) && comparacion.hasOwnProperty('unos') && Type.isNumber(umbral)) {
    return Number(comparacion.unos >= umbral)
  } else return 0
}
