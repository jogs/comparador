const Type = require('../../validators/Type')
const Igualdad = require('./Igualdad')

module.exports = (a, b = a, umbral = 20) => {
  if (Type.isNumber(a) && Type.isNumber(b) && Type.isNumber(umbral)) {
    if (a === b) return Igualdad(a, b)
    return Number((Math.abs(a - b) <= umbral))
  } else return 0
}
