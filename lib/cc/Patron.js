const Type = require('../../validators/Type')
const Igualdad = require('./Igualdad')

module.exports = (a, b = a, patron = '') => {
  if (Type.isString(a) && Type.isString(b) && Type.isString(patron)) {
    if (a === b) return Igualdad(a, b)
    let regexp = new RegExp(patron)
    return Number(regexp.test(a) && regexp.test(b))
  } else return 0
}
