const Type = require('../../validators/Type')
const Interval = require('../../helpers/Intervalo')
const Igualdad = require('./Igualdad')

const SingleInterval = (a, b, intervalo) => {
  let interval = new Interval(intervalo)
  return Number(interval.isIn(a) && interval.isIn(b))
}

module.exports = (a, b = a, intervalo = '[0,1000]') => {
  if (Type.isNumber(a) && Type.isNumber(b)) {
    if (a === b) return Igualdad(a, b)
    if (Type.isString(intervalo)) {
      return SingleInterval(a, b, intervalo)
    } else if (Type.isArray(intervalo)) {
      let result = 0
      intervalo.map((subintervalo) => {
        if (Type.isString(subintervalo)) {
          result = result | SingleInterval(a, b, subintervalo)
        }
      })
      return result
    }
  } else return 0
}
