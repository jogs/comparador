const Set = require('../../helpers/Conjunto')
const Type = require('../../validators/Type')
const Igualdad = require('./Igualdad')

const SingleSet = (a, b, conjunto) => {
  let set = new Set(conjunto)
  return Number(set.incluye(a) && set.incluye(b))
}

module.exports = (a, b = a, conjunto = []) => {
  if (Type.isArray(conjunto)) {
    if (a === b) return Igualdad(a, b)
    if (conjunto.length > 0) {
      if (Type.isArray(conjunto[0])) {
        let result = 0
        conjunto.map((subconjunto) => {
          if (Type.isArray(subconjunto)) {
            result = result | SingleSet(a, b, subconjunto)
          }
        })
        return result
      } else {
        return SingleSet(a, b, conjunto)
      }
    } else return 0
  } else return 0
}
