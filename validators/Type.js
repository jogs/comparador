module.exports = class ValidatorType {
  constructor () {
    //
  }
  static isNumber (data) {
    return (typeof data === 'number')
  }
  static isString (data) {
    return (typeof data === 'string')
  }
  static isNull (data) {
    return (data === null)
  }
  static isEmpty (data) {
    return (data === null || data === [] || data === {})
  }
  static isBoolean (data) {
    return (typeof data === 'boolean')
  }
  static isArray (data) {
    return Array.isArray(data)
  }
  static isObject (data) {
    // console.log(typeof data);
    // console.log(typeof data === 'object', !Array.isArray(data) && data !== null)
    return (typeof data === 'object' && !Array.isArray(data) && data !== null)
  }
}
