const Numero = require('./Numero')
const first = /^\s*(\[|\()\s*/
const last = /\s*(\]|\))\s*$/
const comma = /\s*\,\s*/
module.exports = new RegExp(`${first.source}(${Numero.source})${comma.source}(${Numero.source})${last.source}`)
