const Sentinel = require('../Sentinel')
const Profile = require('./Profile')
const Type = require('../validators/Type')

const functions = (type) => {
  let list = {}
  Sentinel[type].map((func) => {
    list[func.toLowerCase()] = require(`${Sentinel.path[type]}${func}`)
  })
  return list
}

module.exports = class Comparer {
  constructor (schema, cc, sf) {
    this.instance = new Profile(schema)
    this.cc = functions('cc')
    this.sf = functions('sf')
    this.profile = this._validateCC(cc)
    this.similarity = this._validateSF(sf)
  }
  _validateSF (sf) {
    if (Type.isObject(sf)) {
      let sim = {}
      if (sf.hasOwnProperty('funcion')) {
        if (Type.isString(sf.funcion)) {
          sim.funcion = ((Object.keys(this.sf)).includes(sf.funcion.toLowerCase())) ? this.sf[sf.funcion] : this.sf.igualdad
        } else {
          sim.funcion = this.sf.igualdad
        }
      } else {
        sim.funcion = this.sf.igualdad
      }
      if (sf.hasOwnProperty('diferencia')) {
        sim.diferencia = (Type.isBoolean(sf.diferencia)) ? sf.diferencia : false
      } else sim.diferencia = false
      if (sf.hasOwnProperty('datos')) {
        sim.datos = sf.datos
      } else sim.datos = null
      return sim
    } else return null
  }
  _validateCC (cc) {
    if (Type.isObject(cc)) {
      let comp = {}
      for (let key in this.instance.schema) {
        key = key.toLowerCase()
        if (this.instance.schema.hasOwnProperty(key) && cc.hasOwnProperty(key)) {
          comp[key] = {}
          if (cc[key].hasOwnProperty('criterio')) {
            if (Type.isString(cc[key].criterio)) {
              // console.log(cc[key].criterio.toLowerCase())
              comp[key].comparar = (Object.keys(this.cc)).includes(cc[key].criterio.toLowerCase()) ? this.cc[cc[key].criterio.toLowerCase()] : this.cc.igualdad
            } else {
              comp[key].comparar = this.cc.igualdad
            }
          }
          if (cc[key].hasOwnProperty('diferencia')) {
            comp[key].diferencia = Type.isBoolean(comp[key].diferencia) ? comp[key].diferencia : false
          } else {
            comp[key].diferencia = false
          }
          comp[key].datos = (cc[key].hasOwnProperty('datos')) ? cc[key].datos : null
        }
        // console.log(key, comp[key])
      }
      return comp
    } else return null
  }
  _inverse (value, iw = 1) {
    return Math.abs((value * iw) - iw)
  }
  compare (object1, object2) {
    // console.log(this.similarity)
    let o1 = this.instance.of(object1)
    console.log(o1)
    let o2 = this.instance.of(object2)
    console.log(o2)
    if (this.profile !== null) {
      let result = {}
      let zeroCount = 0
      for (let key in this.profile) {
        key = key.toLowerCase()
        // console.log(this.profile[key])
        // console.log(o1[key], o2[key])
        result[key] = 0
        if (this.profile.hasOwnProperty(key) && o1.hasOwnProperty(key) && o2.hasOwnProperty(key)) {
          result[key] = this.profile[key].comparar(o1[key], o2[key], this.profile[key].datos)
        }
        if (this.profile[key].diferencia) result[key] = this._inverse(result[key])
        if (result[key] === 0) zeroCount++
      }
      let resultado = {
        resultado: result,
        ceros: zeroCount,
        unos: Object.keys(result).length - zeroCount
      }
      let similarity = {
        semejanza: this.similarity.funcion(resultado, this.similarity.datos) || 0
      }
      return Object.assign({}, resultado, similarity)
    } else return null
  }
}
