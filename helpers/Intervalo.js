const Type = require('../validators/Type')
const Intervalo = require('../validators/Intervalo')
module.exports = class Interval {
  constructor (string) {
    this.string = string
    this.data = this.parse(string)
  }
  validate (string = this.string) {
    if (Type.isString(string)) {
      return Intervalo.test(string)
    } else return false
  }
  parse (string = this.string) {
    if (this.validate(string)) {
      let tmp = string.match(Intervalo)
      if (tmp.length === 5) {
        return [tmp[1], tmp[2], tmp[3], tmp[4]]
      } else return null
    } else return null
  }
  isValid (string = this.string) {
    return this.validate(string)
  }
  isIn (number) {
    if (Type.isNumber(number) && Type.isArray(this.data)) {
      let isIn = false
      if (this.data[0] === '[') {
        isIn = (this.data[1] <= number)
      } else {
        isIn = (this.data[1] < number)
      }
      if (this.data[3] === ']') {
        isIn = (this.data[2] >= number) && isIn
      } else {
        isIn = (this.data[2] > number) && isIn
      }
      return isIn
    } else return false
  }
}
