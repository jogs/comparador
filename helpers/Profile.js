const Schema = require('./Schema')
const Intervalo = require('./Intervalo')

module.exports = class Profile {
  constructor (schema) {
    this.schema = Schema(schema)
  }
  _validate (data, options) {
    if (typeof data === options.dato) {
      if (options.hasOwnProperty('valores')) {
        if (options.variable === 'real') {
          let inter = new Intervalo(options.valores)
          return (inter.isIn(data)) ? data : null
        } else {
          if (typeof data === 'string') data = data.toLowerCase()
          return (options.valores.includes(data)) ? data : null
        }
      } return data
    } else return null
  }
  of (object) {
    let instance = {}
    for (let key in object) {
      if (object.hasOwnProperty(key) && this.schema.hasOwnProperty(key.toLowerCase())) {
        instance[key.toLowerCase()] = this._validate(object[key], this.schema[key.toLowerCase()])
      }
    }
    return instance
  }
}

// let persona = new Profile([
//   {
//     "nombre": "Sexo",
//     "variable": "k-valente",
//     "valores": ["Masculino", "Femenino"]
//   },
//   {
//     "nombre": "Edad",
//     "variable": "real",
//     "valores": "[0, 120]"
//   },
//   {
//     "nombre": "Activo"
//   }
// ])
//  console.log(persona)

//  console.log(persona.instance({
//    "Sexo": "Masculino",
//    "Edad": 18,
//    "Activo": 'Si'
//  }));
