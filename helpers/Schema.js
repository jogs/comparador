const Type = require('../validators/Type')
const Intervalo = require('../validators/Intervalo')

const variables = ['booleano', 'k-valente', 'real']

const getValidDataType = (variable) => {
  switch (variable) {
    case 'booleano':
      return 'boolean'
    case 'real':
      return 'number'
    default:
      return 'string'
  }
}

const cleanArray = (array) => array.filter(
  (elem, index, self) => index === self.indexOf(elem)
)

const standarize = (array) => array.map(
  (element) => (typeof element === 'string') ? element.toLowerCase() : element
)

module.exports = (schema) => {
  if (Type.isArray(schema)) {
    let validSchema = {}
    schema.map((element) => {
      let obj = {}
      if (element.hasOwnProperty('variable')) {
        if (Type.isString(element.variable)) {
          obj.variable = (variables.includes(element.variable)) ? element.variable.toLowerCase() : 'booleano'
        } else {
          obj.variable = 'booleano'
        }
      } else {
        obj.variable = 'booleano'
      }
      obj.dato = getValidDataType(obj.variable)
      if (element.hasOwnProperty('valores')) {
        switch (obj.variable) {
          case 'booleano':
            obj.valores = [true, false]
            break
          case 'k-valente':
            obj.valores = (Type.isArray(element.valores)) ? standarize(cleanArray(element.valores)) : []
            break
          default:
            obj.valores = (Type.isString(element.valores) && Intervalo.test(element.valores)) ? element.valores : '[0,1]'
        }
      } else if (obj.dato === 'booleano') {
        obj.valores = [true, false]
      }
      validSchema[element.nombre.toLowerCase()] = obj
    })
    return validSchema
  } else return {}
}
