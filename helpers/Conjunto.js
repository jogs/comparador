module.exports = class Conjunto {
  constructor (miembros) {
    this.miembros = miembros.filter(
      (elem, index, self) => index === self.indexOf(elem)
    )
    return this
  }
  añadir (miembro) {
    if (!this.miembros.includes(miembro)) {
      this.miembros.push(miembro)
    }
    return this
  }
  quitar (miembro) {
    let index = this.miembros.indexOf(miembro)
    if (index > -1) {
      this.miembros.splice(index, 1)
    }
    return this
  }
  incluye (miembro) {
    return this.miembros.includes(miembro)
  }
}
