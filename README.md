# Comparador

Este es un comparador de objetos en formato JSON

## [Demo](http://comparar.azurewebsites.net/)

## Instalación

### Requisitos

* [Git](https://git-scm.com/downloads) *
* [Node.JS](https://nodejs.org/es/) >= 6.3

### Descarga

* Via Git

```Bash
git clone https://gitlab.com/jogs/comparador.git
```
* Descargar
  + [ZIP](https://gitlab.com/jogs/comparador/repository/archive.zip?ref=master)
  + [TAR](https://gitlab.com/jogs/comparador/repository/archive.tar?ref=master)
  + [TAR.GZ](https://gitlab.com/jogs/comparador/repository/archive.tar.gz?ref=master)
  + [TAR.BZ2](https://gitlab.com/jogs/comparador/repository/archive.tar.bz2?ref=master)

Descargas disponibles en [Gitlab](https://gitlab.com/jogs/comparador)

### Instalar depenencias

Con la terminal en la raiz del proyecto ingrese:

```Bash
npm i
```

## Ejecutar el servicio

Con la terminal en la raiz del proyecto ingrese:

```Bash
node .
```

## Correr Pruebas

Con la terminal en la raiz del proyecto ingrese:

```Bash
make test
```

Abra su navegador de preferencia e ingrese a __*"http://0.0.0.0:19170"*__

## Uso del API

## Comparación

Envie una solicitud HTTP mediante el metodo POST con Cabeceras Content-Type: application/json a __*"http://0.0.0.0:19170/compare"*__ y en e cuerpo de la de solicitud texto con un formato JSON validos

* Ejempo del JSON

```JSON
{
	"esquema" : [
	    {
	      "nombre": "Sexo",
	      "variable": "k-valente",
	      "valores": ["Masculino", "Femenino"]
	    },
	    {
	      "nombre": "Edad",
	      "variable": "real",
	      "valores": "[0, 120]"
	    },
	    {
	      "nombre": "Activo"
	    }
	  ],
	"comparacion": {
    	"sexo": {
	      "criterio": "igualdad"
	    },
	    "edad": {
	      "criterio": "umbral",
	      "diferencia": true,
	      "datos": 5
    	}
  	},
	"semejanza": {
    	"funcion": "porcentaje",
    	"diferencia": false,
    	"datos": 66.7
  	},
  	"objetos": [
  		{
		  "sexo": "Masculino",
		  "edad": 46
		},
		{
		  "sexo": "masculino",
		  "edad": 18
		}
  	]
}
```

### CURL SHELL

``` bash
curl --request POST \
  --url http://127.0.0.1:19170/compare \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/json' \
  --data '{"esquema":[{"nombre":"Sexo","variable":"k-valente","valores":["Masculino","Femenino"]},{"nombre":"Edad","variable":"real","valores":"[0,120]"},{"nombre":"Activo"}],"comparacion":{"sexo":{"criterio":"igualdad"},"edad":{"criterio":"umbral","diferencia":true,"datos":5}},"semejanza":{"funcion":"porcentaje","diferencia":false,"datos":66.7},"objetos":[{"sexo":"Masculino","edad":46	},{"sexo":"masculino","edad": 18}]}'
```

### CURL

```Bash
curl -X POST -H "Content-Type: application/json" -H "Cache-Control: no-cache" -d '{
	"esquema" : [
	    {
	      "nombre": "Sexo",
	      "variable": "k-valente",
	      "valores": ["Masculino", "Femenino"]
	    },
	    {
	      "nombre": "Edad",
	      "variable": "real",
	      "valores": "[0, 120]"
	    },
	    {
	      "nombre": "Activo"
	    }
	  ],
	"comparacion": {
    	"sexo": {
	      "criterio": "igualdad"
	    },
	    "edad": {
	      "criterio": "umbral",
	      "diferencia": true,
	      "datos": 5
    	}
  	},
	"semejanza": {
    	"funcion": "porcentaje",
    	"diferencia": false,
    	"datos": 66.7
  	},
  	"objetos": [
  		{
		  "sexo": "Masculino",
		  "edad": 46
		},
		{
		  "sexo": "masculino",
		  "edad": 18
		}
  	]
}' "http://127.0.0.1:19170/compare"
```

## Funciones

Envie una solicitud **HTTP** mediante el metodo **GET** a __*"http://0.0.0.0:19170/functions"*__ para recibir el listado de criterios de comparacion y funciones de semejanza disponibles

### CURL

```Bash
curl -X GET -H "Content-Type: application/json" -H "Cache-Control: no-cache" "http://127.0.0.1:19170/functions"
```

## Esquema

El esquema se encarga de definir la estructura de la clase, rasgo a rasgo

El documento se escribe en formato JSON standard [_Vease json.org_](http://www.json.org/)

+ Claves:
  * **nombre:** Describe el nombre del rasgo (es obligatorio)
  * **variable:** Define el tipo de variable del rasgo (default: booleano)
  * **valores:** Define el conjunto de valores que admite el rasgo (se require para validar, si no se ingresa se toma como abierto y no se validara el contenido)

+ _**Ejemplo**_

``` JSON
{
  "esquema" : [
    {
      "nombre": "Sexo",
      "variable": "k-valente",
      "valores": ["Masculino", "Femenino"]
    },
    {
      "nombre": "Edad",
      "variable": "real",
      "valores": "[0, 120]"
    },
    {
      "nombre": "Activo"
    }
  ]
}
```
_Nota: Si no solo se pasa la clave nombre, se interpretara como booleano (true, false)_

## Criterios de Comparación

Los criterios de comparación determinan el parecido de dos rasgos

* Ejemplo

```JSON
{
  "comparacion": {
    "sexo": {
      "criterio": "igualdad"
    },
    "edad": {
      "criterio": "umbral",
      "diferencia": true,
      "datos": 5
    }
  }
}
```

### Criterios disponibles

* Igualdad

Hace una comparación de igualdad estricta (el texto no es sensible a mayusculas y minusculas)

```JSON
{
  "criterio": "igualdad"
}
```

* Intervalo

Hace una comparación por intervalos numericos, utiliza notación de intervalos
"[0,100]" implica todos los valores entre 0 y 100
"(0,100)" implica todos los valores mayores a 0 y menores a 100
"(0,100]" implica todos los valores mayores a 0 y menores o iguales a 100
"[0,100)" implica todos los valores mayores o iguales a 0 y menores a 100

```JSON
{
  "criterio": "intervalo",
  "datos": ["[0,100)", "[100,200)"]
}
```

* Umbral

Hace una compración entre dos valores numericos, donde si |x - y| >= umbral regresara 1, en otro caso regresara 0

```JSON
{
  "criterio": "Umbral",
  "datos": 25
}
```

* Conjunto

Hace una comparación donde ambos elementos a comparar deben pertenecer a un mismo conjunto de datos

```JSON
{
  "criterio": "conjunto",
  "datos": [["a","e","i","o","u"],["x","y","z"]]
}
```

* Patron

Hace una comparación donde ambos elementos deben ser de tipo texto, y coincidir con una expresion regural en formato javascript

```JSON
{
  "criterio": "patron",
  "datos": "^h[\\w\\s]*a$"
}
```

### Criterio por diferencia

Si necesita resaltar la diferencia solo necesita definir el atributo diferencia  y el valore true, al objeto de configuración

```JSON
{
  "criterio": "igualdad",
  "diferencia": true
}
```

_Nota: Para este ejemplo ambos deben comenzar con h, terminar con a y entre estos valores acepta cualquier cantidad de texto y espacios_

### Añadir nuevos criterios

1. Cree un nuevo archivo con extencion "js" en la carpeta lib/cc
2. Importe la biblioteca de validacion con "const Type = require('../../validators/Type')"
3. Escriba una función pura con con la siguiente estructura (o1, o2, options) => compare (Una función pura es aquella que dado un conjunto de datos siempre respondera de la misma forma, ejemplo una suma, una funcion impura son las generadoras de valores aleatorios)

* Ejemplo: Comparar la longitud de dos textos con umbral

```javascript
const Type = require('../../validators/Type')

/*
** recibe o1, o2, y umbral
** si o2 no es definido tomara el valor de o1
** si el umbral no se define sera igual a 0
*/
module.exports = (o1, o2 = o1, umbral = 0) => {
  /*
  ** Se validan los tipos de datos, si no son validos automaticamente
  ** se regresa 0
  */
  if (Type.isString(o1) && Type.isString(o2) && Type.isNumber(umbral)) {
    /*
    ** Si el valor absoluto de la resta de las longitudes de las cadenas
    ** es menor o igual a el valor absoluto del umbral (para validar)
    ** El resultado booleano de esta comparacion se transforma a numero
    ** donde true => 1 y false => 0
    */
    return Number(Math.abs(o1.length - o2.length) <= Math.abs(umbral))
  } else return 0
}
```

_Nota: La nueva funcion (Criterio de comparación) sera indexada automaticamente y estara disponible para ser utilizada_

4. Escriba Pruebas unitarias (opcional, usted puede escribir las pruebas antes que la funcíon al estilo BDD)
4.1. Cree un nuevo archivo en la carpeta test con el nombre de la funcion creada seguido de .CC.Spec.js (no es obligatorio pero se suguiere para poder idenficar la naturaleza del archivo), ejemplo Longitud.CC.Spec.js
4.2 Importe la funcion que creeo
4.3 Ingrese una descrion
4.4 Escriba las pruebas

* Ejemplo

```javascript
const Longitud = require('../lib/cc/Longitud')

describe('Criterio de comparación por Longitud usando umbrales', () => {
  /*
  ** Escribiremos pruebas separadas para cada tipo de datos basico
  */
  describe('Boolean', () => {
    /*
    ** Debe regresar cero por que el tipo de dato no es aceptado
    */
    it('Deberia regresar 0 al comparar true con true', () => {
      expect(Longitud(true, true)).to.equal(0)
    })
    it('Deberia regresar 0 al comparar true con false', () => {
      expect(Longitud(true, false)).to.equal(0)
    })
  })
  describe('String', () => {
    /*
    ** Al no definir umbral las cadenas deben tener la misma longitud
    */
    it('Deberia regresar 1 al comparar Ejemplo1 con Ejemplo2', () => {
      expect(Longitud('Ejemplo1', 'Ejemplo2')).to.equal(1)
    })
    it('Deberia regresar 0 al comparar Ejemplo3 con Ejemplo 3', () => {
      expect(Longitud('Ejemplo3', 'Ejemplo 3')).to.equal(0)
    })
    /*
    ** Al definir un umbral ya no se comparan longitudes identicas
    */
    it('Deberia regresar 1 al comparar Ejemplo3 con Ejemplo 3', () => {
      expect(Longitud('Ejemplo3', 'Ejemplo 3', 5)).to.equal(1)
    })
  })
  describe('Number', () => {
    /*
    ** Debe regresar cero por que el tipo de dato no es aceptado
    */
    it('Deberia regresar 0 al comparar 100 con 100', () => {
      expect(Longitud(100, 100)).to.equal(0)
    })
    it('Deberia regresar 0 al comparar 120 con 120', () => {
      expect(Longitud(120, 120)).to.equal(0)
    })
  })
})
```

5. Ejecute las pruebas, al tener una terminal abierta posicionada en la raiz del proyecto escriba "make test" y automaticamente se ejecutaran las pruebas mistras el proceso este activo cualquier cambio hecho en el proyecto refresacara el test

## Funciones de Semejanza

Las funciónes de semejanza determinan que tan parecidos son entre si 2 objetos

* Ejemplo

```JSON
{
  "semejanza": {
    "funcion": "porcentaje",
    "diferencia": false,
    "datos": 66.7
  }
}
```

## Objetos

Los objetos requieren como minimo las mismas claves que los criterios de comparación

* Ejemplo

_Objeto 1_

```JSON
{
  "sexo": "Masculino",
  "edad": 46
}
```
_Objeto 2_

```JSON
{
  "sexo": "masculino",
  "edad": 18
}
```
