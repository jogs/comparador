const express = require('express')
const helmet = require('helmet')
const cors = require('cors')
const bodyParser = require('body-parser')
const network = require('os').networkInterfaces()
const Comparer = require('./helpers/Comparer')

const PORT = process.env.port || 19170

const app = express()

app.use(helmet())
app.use(helmet.hidePoweredBy({ setTo: 'Skypunch' }))

app.use(cors())

app.use(bodyParser.json())

app.use(express.static(`${__dirname}/public`))

app.use((req, res, next) => {
  console.log(`${req.method} -> ${req.path} (at ${Date()})`)
  next()
  console.log('========================================================')
})

app.route('/')
.get((req, res) => {
  res.status(200)
  .type('html')
  .sendFile(`${__dirname}/public/index.html`)
})

app.route('/functions')
.get((req, res) => {
  const Sentinel = require('./Sentinel')
  res.status(200)
  .json({
    body: {
      cc: Sentinel.cc,
      sf: Sentinel.sf
    }
  })
})

app.route('/compare')
.post((req, res) => {
  if (req.is('json')) {
    if (req.body.hasOwnProperty('esquema') && req.body.hasOwnProperty('comparacion') && req.body.hasOwnProperty('semejanza') && req.body.hasOwnProperty('objetos')) {
      let comparador = new Comparer(req.body.esquema, req.body.comparacion, req.body.semejanza)
      let resultado = comparador.compare(req.body.objetos[0], req.body.objetos[1])
      if (resultado !== null) {
        res.status(201)
        .json({
          body: resultado
        })
      } else {
        res.status(400)
        .json({
          statusText: 'Bad Request: Por favor verifique la documentación, las claves son correctas, pero el contenido no'
        })
      }
    } else {
      res.status(400)
      .json({
        statusText: 'Bad Request: Por favor verifique la documentación'
      })
    }
  } else {
    res.status(415)
    .json({
      statusText: 'Media-Type Error: Se Esperaba JSON'
    })
  }
})

app.use((req, res, next) => {
  res.status(404)
  .json({
    statusText: 'No Encontrado'
  })
})
app.use((err, req, res, next) => {
  res.status(500)
  .json({
    statusText: 'Error Interno de servidor',
    description: err
  })
})

app.listen(PORT, () => {
  console.log('Aplicacion corriendo en:')
  for (let adapter in network) {
    if (network.hasOwnProperty(adapter)) {
      console.log(`\thttp://${network[adapter][0].address}:${PORT}`)
    }
  }
})
