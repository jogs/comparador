const Umbral = require('../lib/cc/Umbral')

describe('Criterio de Comparación por Umbral', () =>{
  describe('Boolean', () => {
    it('Deberia regresar 0 al comparar true con true', () => {
      expect(Umbral(true, true, 1)).to.equal(0)
    })
    it('Deberia regresar 0 al comparar true con false', () => {
      expect(Umbral(true, false, 1)).to.equal(0)
    })
  })
  describe('String', () => {
    it('Deberia regresar 0 al comparar "hola" con "hola"', () => {
      expect(Umbral("hola", "hola", 1)).to.equal(0)
    })
  })
  describe('Number', () => {
    it('Deberia regresar 1 al comparar 3 con 3, umbral 0', () => {
      expect(Umbral(3, 3, 0)).to.equal(1)
    })
    it('Deberia regresar 1 al comparar 3 con 8, umbral 5', () => {
      expect(Umbral(3, 8, 5)).to.equal(1)
    })
    it('Deberia regresar 0 al comparar 3 con 8, umbral 4.9', () => {
      expect(Umbral(3, 8, 4.9)).to.equal(0)
    })
    it('Deberia regresar 0 al comparar 3 con 3, umbral "cero"', () => {
      expect(Umbral(3, 3, 'x')).to.equal(0)
    })
  })
})
