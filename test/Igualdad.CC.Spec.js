const Igualdad = require('../lib/cc/Igualdad')

describe('Criterio de Comparación por Igualdad', () => {
  it('La funcion debe regresar un numero', () => {
    expect(Igualdad(1, 1)).to.a('number')
  })
  describe('Boolean', () => {
    it('Debe Regresar 1 al comparar true con true', () => {
      expect(Igualdad(true, true)).to.equal(1)
    })
    it('Debera Regresar 0 al comparar true con false', () => {
      expect(Igualdad(true, false)).to.equal(0)
    })
    it('Debera Regresar 0 al comparar true con "true"', () => {
      expect(Igualdad(true, 'true')).to.equal(0)
    })
  })
  describe('String', () => {
    it('deberia regresar 1 al comparar hola con hola', () => {
      expect(Igualdad('hola', 'hola')).to.equal(1)
    })
    it('deberia regresar 0 al comparar hola con h0la',() => {
      expect(Igualdad('hola','h0la')).to.equal(0)
    })
    it('deberia regresar 0 al comparar hola con HoLa',() => {
      expect(Igualdad('hola','HoLa')).to.equal(0)
    })
    it('deberia regresar 0 al comparar hola con ola',() => {
      expect(Igualdad('hola','ola')).to.equal(0)
    })
    it('deberia regresar 0 al comparar hola con Hola',() => {
      expect(Igualdad('hola','Hola')).to.equal(0)
    })
    it('deberia regresar 0 al comparar hola con HOLa',() => {
      expect(Igualdad('hola','HOLa')).to.equal(0)
    })
  })
  describe('Number', () => {
    it('deberia regresar 1 al comparar 5 con 5',() => {
      expect(Igualdad(5, 5)).to.equal(1)
    })
    it('deberia regresar 0 al comparar 5 con .5',() => {
      expect(Igualdad(5, 0.5)).to.equal(0)
    })
    it('deberia regresar 1 al comparar 5 con 5.',() => {
      expect(Igualdad(5, 5.0)).to.equal(1)
    })
    it('deberia regresar 0 al comparar 5 con 5.01',() => {
      expect(Igualdad(5, 5.01)).to.equal(0)
    })
  })
})
