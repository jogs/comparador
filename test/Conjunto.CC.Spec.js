const Conjunto = require('../lib/cc/Conjunto')

describe('Criterio de Comparación por Conjunto', () =>{
  describe('Boolean', () => {
    it('Deberia regresar 1 al comparar true con true, conjunto []', () => {
      expect(Conjunto(true, true, [])).to.equal(1)
    })
    it('Deberia regresar 0 al comparar true con false, conjunto []', () => {
      expect(Conjunto(true, false, [])).to.equal(0)
    })
    it('Deberia regresar 1 al comparar true con false, conjunto [true, false]', () => {
      expect(Conjunto(true, false, [true, false])).to.equal(1)
    })
  })
  describe('String', () => {
    it('Deberia regresar 1 al comparar hola con hola, conjunto []', () => {
      expect(Conjunto('hola', 'hola', [])).to.equal(1)
    })
    it('Deberia regresar 1 al comparar hola con mundo, conjunto ["hola", "mundo", "!"]', () => {
      expect(Conjunto('hola', 'mundo', ["hola", "mundo", "!"])).to.equal(1)
    })
    it('Deberia regresar 1 al comparar hola con mundo, conjunto []', () => {
      expect(Conjunto('hola', 'mundo', [])).to.equal(0)
    })
    it('Deberia regresar 1 al comparar hola con mundo, conjunto [["hola", "mundo"], ["hello", "world"]]', () => {
      expect(Conjunto('hola', 'mundo', [["hola", "mundo"], ["hello", "world"]]))
      .to.equal(1)
    })
    it('Deberia regresar 1 al comparar hola con mundo, conjunto [["hello", "world"], ["hello", "world"]]', () => {
      expect(Conjunto('hello', 'world', [["hola", "mundo"], ["hello", "world"]]))
      .to.equal(1)
    })
    it('Deberia regresar 0 al comparar hola con mundo, conjunto [["hello", "world"], ["hello", "world"]]', () => {
      expect(Conjunto('hello', 'hola', [["hola", "mundo"], ["hello", "world"]]))
      .to.equal(0)
    })
  })
  describe('Number', () => {

  })
})
