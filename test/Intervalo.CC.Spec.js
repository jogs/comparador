const Intervalo = require('../lib/cc/Intervalo')

describe('Criterio de Comparación por Intervalo', () =>{
  describe('Boolean', () => {
    it('Deberia regresar 0 al comparar true con true', () => {
      expect(Intervalo(true, true, 1)).to.equal(0)
    })
    it('Deberia regresar 0 al comparar true con false', () => {
      expect(Intervalo(true, false, 1)).to.equal(0)
    })
  })
  describe('String', () => {
    it('Deberia regresar 0 al comparar hola con hola', () => {
      expect(Intervalo('hola', 'hola')).to.equal(0)
    })
  })
  describe('Number', () => {
    it('Deberia regresar 1 al comparar 0 con 1, intervalo "[0,1]"', () => {
      expect(Intervalo(0, 1, '[0,1]')).to.equal(1)
    })
    it('Deberia regresar 1 al comparar 0 con 1, intervalo ["[0,1]", "(1,2]"]', () => {
      expect(Intervalo(0, 1, ['[0,1]', '(1,2]'])).to.equal(1)
    })
    it('Deberia regresar 0 al comparar 0 con 1, intervalo "(0,1]"', () => {
      expect(Intervalo(0, 1, '(0,1]')).to.equal(0)
    })
    it('Deberia regresar 0 al comparar 0 con 1, intervalo "[0,1)"', () => {
      expect(Intervalo(0, 1, '[0,1)')).to.equal(0)
    })
    it('Deberia regresar 0 al comparar 0 con 1, intervalo "(0,1)"', () => {
      expect(Intervalo(0, 1, '(0,1)')).to.equal(0)
    })
    it('Deberia regresar 0 al comparar 0 con 1, intervalo "(a,b)"', () => {
      expect(Intervalo(0, 1, '(a,b)')).to.equal(0)
    })
  })
})
