const Patron = require('../lib/cc/Patron')

describe('Criterio de Comparación por Patron', () =>{
  describe('Boolean', () => {
    it('Deberia regresar 0 al comparar true con true', () => {
      expect(Patron(true, true)).to.equal(0)
    })
    it('Deberia regresar 0 al comparar true con false', () => {
      expect(Patron(true, false)).to.equal(0)
    })
  })
  describe('String', () => {
    it('Deberia regresar 1 al comparar hola con hola', () => {
      expect(Patron('hola', 'hola')).to.equal(1)
    })
    it('Deberia regresar 1 al comparar hola con hola, patron "^h"', () => {
      expect(Patron('hola', 'hello', '^h')).to.equal(1)
    })
    it('Deberia regresar 1 al comparar hola con hola, patron "ola$"', () => {
      expect(Patron('hola', 'ola', 'ola$')).to.equal(1)
    })
    it('Deberia regresar 1 al comparar hola con hola, patron ""', () => {
      expect(Patron('hola', 'ola', '')).to.equal(1)
    })
  })
  describe('Number', () => {
    it('Deberia regresar 0 al comparar 6 con 6', () => {
      expect(Patron(6, 6)).to.equal(0)
    })
    it('Deberia regresar 0 al comparar 6 con 8', () => {
      expect(Patron(6, 8)).to.equal(0)
    })
  })
})
